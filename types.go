package gosmsapi

import (
	"fmt"
	"strings"
)

// Request - SMSAPI api request
// for more info, read https://docs.smsapi.com/?shell#2-single-sms
type Request struct {
	To              string  `json:"to,omitempty"`
	Group           string  `json:"group,omitempty"`
	Message         string  `json:"message,omitempty"`
	From            string  `json:"from,omitempty"`
	Encoding        string  `json:"encoding,omitempty"`
	Flash           *uint8  `json:"flash,omitempty"`
	Test            *uint8  `json:"test,omitempty"`
	Details         *uint8  `json:"details,omitempty"`
	Date            string  `json:"date,omitempty"`
	DateValidate    *uint8  `json:"date_validate,omitempty"`
	Datacoding      string  `json:"datacoding,omitempty"`
	UDH             string  `json:"udh,omitempty"`
	AllowDuplicates *uint8  `json:"allow_duplicates,omitempty"`
	IDX             *uint64 `json:"idx,omitempty"`
	CheckIDX        *uint8  `json:"check_idx,omitempty"`
	MaxParts        *uint   `json:"max_parts,omitempty"`
	Normalize       *uint8  `json:"normalize,omitempty"`
	Fast            *uint8  `json:"fast,omitempty"`
	ExpirationDate  string  `json:"expiration_date,omitempty"`
	NotifyURL       string  `json:"notify_url,omitempty"`
	DelSchIDs       string  `json:"sch_del,omitempty"`
	Format          string  `json:"format,omitempty"`
}

// Response - SMSAPI api response
type Response struct {
	Count uint `json:"count"`
	Info  []struct {
		ID              string  `json:"id"`
		Points          float64 `json:"points"`
		Number          string  `json:"number"`
		DateSent        uint64  `json:"date_sent"`
		SubmittedNumber string  `json:"submitted_number"`
		Status          string  `json:"status"`
		Error           *uint64 `json:"error"`
		IDX             *uint64 `json:"idx"`
	} `json:"list"`
	Message string `json:"message"`
	Length  uint   `json:"length"`
	Parts   uint   `json:"parts"`
}

// ResponseError - SMSAPI api response error
type ResponseError struct {
	ErrorCode      uint   `json:"error"`
	Message        string `json:"message"`
	InvalidNumbers []struct {
		Number          string `json:"number"`
		SubmittedNumber string `json:"submitted_number"`
		Message         string `json:"message"`
	} `json:"invalid_numbers"`
}

func (e ResponseError) Error() string {
	badNumbers := make([]string, len(e.InvalidNumbers))
	for _, n := range e.InvalidNumbers {
		badNumbers = append(badNumbers, fmt.Sprintf("number %q, number error message: %q", n.SubmittedNumber, n.Message))
	}

	return fmt.Sprintf(
		"error sending messages, error code: %d, error message: %q, invalid numbers: %s",
		e.ErrorCode,
		e.Message,
		strings.Join(badNumbers, ", "),
	)
}
