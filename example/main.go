package main

import (
	"context"
	"crypto/tls"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"gitlab.com/boromil/gosmsapi"
)

func main() {
	// you need to set this env yourself
	accessToken := os.Getenv("SMSAPI_ACCESS_TOKEN")
	httpClient := &http.Client{
		Transport: &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}},
	}
	smsAPIClient := gosmsapi.NewClient(httpClient, accessToken, true)
	sendDate := time.Now().Add(time.Minute).Unix()
	req := gosmsapi.Request{
		To:      "+48500500500",
		Message: "this is a test",
		Date:    strconv.FormatInt(sendDate, 10),
	}
	appCtx, cancelCtx := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelCtx()

	resp, err := smsAPIClient.Send(appCtx, req)
	switch er := err.(type) {
	// check if the error is of gosmsapi.ResponseError type
	// and if yes, we can get more precise info
	case gosmsapi.ResponseError:
		log.Fatalf(
			"response error: code: %d, msg: %q, bad numbers: %+v\n",
			er.ErrorCode,
			er.Message,
			er.InvalidNumbers,
		)
	case error:
		log.Fatalf("%v\n", err)
	default:
		log.Printf("%+v\n", resp)
	}
}
