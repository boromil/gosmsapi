package gosmsapi

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/pkg/errors"
)

const (
	// DefaultAPIURL - default SMSAPI API url
	DefaultAPIURL = "https://api.smsapi.com/sms.do"
	// BackupAPIURL - backup SMSAPI API url
	BackupAPIURL = "https://api2.smsapi.com/sms.do"
	// DefaultFormat - default SMSAPI response format
	DefaultFormat = "json"
	// DefaultEncoding - default SMSAPI message encoding
	DefaultEncoding = "utf-8"
)

// Doer - represents a http.Client
type Doer interface {
	Do(req *http.Request) (*http.Response, error)
}

// Client - the main SMSAPI client
type Client struct {
	apiURL      string
	accessToken string
	testing     bool

	hc Doer
}

// NewClient - default SMSAPI client constructor
func NewClient(client Doer, accessToken string, testing bool) *Client {
	return &Client{
		apiURL:      DefaultAPIURL,
		accessToken: accessToken,
		testing:     testing,
		hc:          client,
	}
}

// SetAPIURL - sets a custom SMSAPI API url
func (c *Client) SetAPIURL(apiURL string) error {
	if _, err := url.Parse(apiURL); err != nil {
		return err
	}
	c.apiURL = apiURL

	return nil
}

// SetAccessToken - sets a new SMSAPI API access token
func (c *Client) SetAccessToken(accessToken string) {
	c.accessToken = accessToken
}

// SetClient - sets a custom http.Client
func (c *Client) SetClient(client Doer) {
	c.hc = client
}

func (c *Client) doReq(ctx context.Context, r Request) (Response, error) {
	if c.testing {
		t := uint8(1)
		r.Test = &t
	}
	r.Format = DefaultFormat

	buf := bytes.NewBuffer([]byte{})
	if err := json.NewEncoder(buf).Encode(r); err != nil {
		return Response{}, errors.Wrap(err, "json.Encode")
	}

	req, err := http.NewRequest(http.MethodPost, c.apiURL, buf)
	if err != nil {
		return Response{}, errors.Wrap(err, "http.NewRequest")
	}
	req = req.WithContext(ctx)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+c.accessToken)

	resp, err := c.hc.Do(req)
	if resp != nil && resp.Body != nil {
		defer func() {
			io.Copy(ioutil.Discard, resp.Body)
			resp.Body.Close()
		}()
	}
	if err != nil {
		return Response{}, errors.Wrap(err, "Do")
	}

	if resp.StatusCode/100 != 2 {
		return Response{}, errors.New("expected an 20x response, got " + resp.Status)
	}

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return Response{}, errors.Wrap(err, "ioutil.ReadAll")
	}

	var rerr ResponseError
	if err := json.NewDecoder(bytes.NewReader(b)).Decode(&rerr); err == nil && rerr.ErrorCode != 0 {
		return Response{}, rerr
	}

	var rd Response
	if err := json.NewDecoder(bytes.NewReader(b)).Decode(&rd); err != nil {
		return Response{}, errors.Wrap(err, "json.Decode")
	}

	return rd, nil
}

// Send - does a request to the SMSAPI api for a message to be sent or put on the schedule
func (c *Client) Send(ctx context.Context, r Request) (Response, error) {
	if r.Encoding == "" {
		r.Encoding = DefaultEncoding
	}

	return c.doReq(ctx, r)
}

// DeleteScheduled - does a request to the SMSAPI api for a scheduled message to be removed from the schedule
func (c *Client) DeleteScheduled(ctx context.Context, ids ...string) (Response, error) {
	return c.doReq(ctx, Request{DelSchIDs: strings.Join(ids, ",")})
}
